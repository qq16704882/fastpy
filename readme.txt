高性能python http服务器+web协程框架
支持linux windows 平台 由于没钱买mac，还没测
c++版本请参考https://git.oschina.net/feimat/fastrpc

源代码只有800多行

性能比较如下

tornado 4kqps 多进程1wqps 
nginx+tornado 9kqps
nginx+uwsgi 8kqps （注意：没说比nginx快，只是这几个web框架不行） 

django和webpy 性能不是一个级别的

本server 2w qps 
没用任何python加速

已经真实使用到自己的多个项目之中，效果明显
欢迎加入it民工qq群339711102，一起探讨优化哦



用户文档：

1、启动：
   指定监听端口即可启动
   python fastpy.py 8992 
   （如果需要使用gevent协程功能请先安装gevent，要求版本1.0以上）

2、快速编写cgi,支持运行时修改,无需重启server

   在fastpy.py同一目录下
   随便建一个python 文件
   例如:
   example.py:
   #-*- coding:utf-8 -*-
   import sys
   #定义一个同名example类
   #定义一个tt函数：
   reload(sys)
   sys.setdefaultencoding('utf8')
   FastpyAutoUpdate=True
   class example():
       def tt(self, request, response_head):
           #print request.form
           #print request.getdic
           #fileitem = request.filedic["upload_file"]
           #fileitem.filename
           #fileitem.file.read()
           return "ccb"+request.path

   则访问该函数的url为 http://ip:port/example.tt
   修改后保存，即可访问，无需重启
   FastpyAutoUpdate 属性可控制需不需要热部署
   FastpyAutoUpdate=true 支持热部署，修改无需重启
   FastpyAutoUpdate=false 则不支持热部署，修改需要重启
   tt函数必须带两个参数
   request：表示请求的数据 默认带以下属性
      headers: 头部 （字典）
      form:  post参数，包括form表单 （字典）
      getdic: url参数 （字典）
      filedic: form表单中文件 （字典）
      rfile: 原始http content内容  （字符串）
      action: python文件名 (这里为example)
      method: 函数方法    （这里为tt）
      command:  （get or post）
      path: url （字符串）
      http_version: http版本号 （http 1.1）
   response_head: 表示response内容的头部
      例如如果要返回用gzip压缩
      则增加头部
      response_head["Content-Encoding"] = "gzip"
      
    除了直接return作为response内容外
    fastpy还
    支持 request.ret(500，res) 的方式返回错误码和内容，
    支持 request.printChunk("print subcontent: xxx") 返回chunk局部内容
         request.flushChunk()结束chunk返回
        
    在WithGevent目录里 我们提供了协程方式的例子，
    将同步的调用（如访问mysql和发http等）变成异步（为什么这样读者可以查询gevent spawn原理， 
    里面提供协程下转发http和数据库连接池的例子）
    
    cgi所有使用例子:
    sample.py  上传文件和form表单使用等基本api的例子
    example.py 使用单例模式和线程+异步返回的例子
    WithGevent/dbsample.py  使用gevent+pymysql实现异步读写数据库的例子（gevent下线程池实现,更多gevent使用请自行百度）
    WithGevent/sample.py    使用gevent实现异步发送http请求的例子
    sendfile/sendfile.py         多线程文件上传服务端代码
    sendfile/sendfile_client.py  多线程文件上传客户端代码
    proxy_server/proxy.py        正向代理服务器代码
    跨平台/   跨平台版本的fastpy.py 
    
    注意在同时使用gevent和redis连接池时请注意以下问题：
    http://stackoverflow.com/questions/10656953/redis-gevent-poor-performance-what-am-i-doing-wrong/10663498#10663498
    
    注意pymysql 连接mycat主从负载时，默认autocommit为false，要改为true才能正常使用 mycat的主从负载

3、支持超大文件上传下载
   默认静态文件(包括html，js、css、图片、文件等)放在static文件夹下
   html和js、css会自动压缩加速
   例如把a.jpg放到static文件夹下
   访问的url为 http://ip:port/static/a.jpg
   支持etag 客户端缓存功能
   (server 使用sendfile进行文件发送，不占内存且快速)

4、支持网页模板编写
   模版引擎代码只有十几行 在WithGevent/common/core.py 文件里的
   class FeimaTpl
   模版用法很简单
   1、用于写python代码的控制块 <% for item in data { %>
   <% %> 中间支持python的 if else for while等程序控制块，
   不同是模版用{ }来代替python 晦涩的缩进来区分控制块范围
   2、取值块 <%=item["id"]> 
   <%= %> 里写的是python的变量指即可，可在1中控制块内使用
   
   下面看个例子
   创建一个模板 a.html
    <html>
        <HEAD><TITLE><%=title%></TITLE></HEAD>
        <BODY>
            <% for item in data{ %>
            <%=item["id"]%>,<%= item["name"] %>
            array data:
            <% for i in item["array"] {%><%= i %><%}%>
            </br>
            <%}%>
        </BODY>
    </html>

   则对应的使用
   from common import core
   tpl = core.FeimaTpl(filepath="./a.html")
   d = []
   d.append({"id":1,"name":"name1","array":[2,4,5,6]})
   d.append({"id":2,"name":"name2","array":[1,3,5,7,9]})
   tpl.assign("title", "my title")
   tpl.assign("data", d)
   print tpl.render()
   则生成：
    <html>
        <HEAD><TITLE>my title</TITLE></HEAD>
        <BODY>
            
            1,name1
            array data:
            2456
            </br>
            
            2,name2
            array data:
            13579
            </br>
            
        </BODY>
    </html>

5、支持http/https透明代理
   python proxy.py 8995
   启动后再浏览器配置代理即可使用，可以弥补nginx 不支持https代理的不足